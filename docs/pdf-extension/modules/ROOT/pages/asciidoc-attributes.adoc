= AsciiDoc Attributes

Built-in and custom AsciiDoc document attributes can be applied to each compiled AsciiDoc document when the PDF extension is generating your PDFs.

IMPORTANT: On Windows, if the attribute value begins with `&` and the command starts with `bundle exec`, a space will be inserted at the start of the value.
There's no other way we know of to prevent the cmd.exe from choking on the attribute argument in this scenario.
It is still possible to use an Asciidoctor extension to trim the attribute value once inside of the Asciidoctor runtime.

NOTE: If any attribute that Assembler passes to the command contains a non-ASCII character, you must enable UTF-8 support on Windows or else the command will fail.
See https://akr.am/blog/posts/using-utf-8-in-the-windows-terminal[Using UTF-8 in the Windows Terminal] to learn different ways of activating this support.
It may also be necessary to set the code page in the terminal to UTF-8 using `chcp 65001`.

== Assign attributes in antora-assembler.yml

AsciiDoc document attributes set in [.path]_antora-assembler.yml_ to supplement attributes defined in the playbook, component version descriptors, and pages.
See xref:antora:playbook:asciidoc-attributes.adoc[] for precedence rules.
Attributes are set and assigned in [.path]_antora-assembler.yml_ just as they are in the playbook or component version descriptors.
Attributes are mapped under the `asciidoc` and `attributes` keys.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    secnums: ''
    xrefstyle: full
    outlinelevels: '2'
----

Attributes assigned in [.path]_antora-assembler.yml_ are applied to all the PDFs according to the xref:antora:playbook:asciidoc-attributes.adoc#precedence-rules[attribute precedence rules].

== Image references

If the name of a document attribute ends with `-image` (e.g., `title-logo-image`), and the value of that attribute is an image macro (e.g., `\image:the-component::logo.png[]`, Assembler will automatically resolve the target as an Antora reference, copy that file into the build directory, and rewrite the target of the macro so the converter (e.g., Asciidoctor PDF) can locate it.
In other words, if the document attribute ends with `-image`, you can use an image macro as you would in Antora.

Since Assembler runs outside the context of any component, the reference must be fully qualified, meaning it must start with a component name.

Here's an example of how to set the logo image on the title page when using Asciidoctor PDF to generate the PDF:

.antora-assembler.yml
[,yaml]
----
asciidoc:
  attributes:
    title-logo-image: image:the-component::logo.png[pdfwidth=1in]
----

Refer to https://docs.asciidoctor.org/pdf-converter/latest/title-page/#logo[Logo image] to learn more about how to use the title logo image with Asciidoctor PDF.

== Generate PDFs with a TOC

To add a Table of Contents to each generated PDF, set the `toc` attribute in [.path]_antora-assembler.yml_.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    toc: ''
----

== Activate a custom PDF theme

The `pdf-theme` attribute is used to activate and apply a custom theme to your PDFs.
The `pdf-theme` attribute accepts the name of a YAML file stored in your playbook directory.
In this example, the file is named [.path]_pdf-theme.yml_.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    pdf-theme: ./pdf-theme.yml
----

== Conditional content

PDF isn't the only output format that Assembler can generate.
That's a choice of the build command (and, in the future, multiple build commands).
Therefore, when Assembler is putting together the aggregate document, the backend is not set to `pdf` as you might expect.
Rather, the backend is still `html5`.

In other words, there are multiple parsing phases.
The first phase is when Assembler is producing the aggregate AsciiDoc document.
During the phase, the backend is still `html5`.
The second phase is when Assembler converts the aggregate document to the output format, such as PDF.
During this phase, the backend corresponds to the output format, such as `pdf`.

If you want to include or filter out content when Assembler is producing the aggregate document, you cannot rely on the backend attribute.
Instead, you need to make use of the built-in `loader-assembler` attribute (or any other attribute you set in the Assembler configuration file).
The `loader-assembler` attribute is set in both phases.

Thus, if you only want to include content in the PDF, you can test for the `loader-assembler` attribute as follows:

[,asciidoc]
----
\ifdef::loader-assembler[]
This content will only appear in the PDF.
\endif::[]
----

To exclude content in the PDF, you can test for the absense of the `loader-assembler` attribute as follows:

[,asciidoc]
----
\ifndef::loader-assembler[]
This content will only appear in the site.
\endif::[]
----

If you want to include content in aggregate document, but not when converting to PDF, then you can test for `backend-pdf` inside this conditional.

[,asciidoc]
----
\ifdef::loader-assembler[]
\ifndef::backend-pdf[]
This content will only appear in the assembled documented, but not when converting to PDF.
\ifndef::[]
\endif::[]
----

Other permutations are available as well.
