'use strict'

const XML_TAG_RX = /<[^>]+>/g
const XML_SPECIAL_CHARS = { '&lt;': '<', '&gt;': '>', '&amp;': '&' }
const XML_SPECIAL_CHARS_RX = /&(?:[lg]t|amp);/g

function sanitize (str) {
  if (~str.indexOf('<')) str = str.replace(XML_TAG_RX, '')
  if (~str.indexOf('&')) str = str.replace(XML_SPECIAL_CHARS_RX, (m) => XML_SPECIAL_CHARS[m])
  return str
}

module.exports = sanitize
