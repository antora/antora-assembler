= The Component
:revnumber: 1.0
:doctype: book
:underscore: _
:page-component-name: the-component
:page-component-version: 1.0
:page-version: {page-component-version}
:page-component-display-version: 1.0
:page-component-title: The Component

:docname: the-page
:page-module: ROOT
:page-relative-src-path: the-page.adoc
:page-origin-url: https://github.com/acme/the-component
:page-origin-start-path:
:page-origin-refname: v1.0
:page-origin-reftype: branch
:page-origin-refhash: a00000000000000000000000000000000000000z
[#the-page:::]
== The Page Title

You'll find <<the-page:::_table_id>> in <<the-page:::_section_title>>.

[discrete#the-page:::_section_title]
=== Section Title

.Table Title
[#the-page:::_table_id]
|===
|foo |bar |baz
|===

In xref:the-page:::_table_id[xrefstyle=short], you can find some random words.
