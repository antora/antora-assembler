'use strict'

const { assert, assertx, describe, it } = require('@antora/assembler-test-harness')
const unconvertInlineAsciiDoc = require('#unconvert-inline-asciidoc')

describe('unconvertInlineAsciiDoc()', () => {
  it('should convert empty string', () => {
    const input = ''
    assertx.empty(unconvertInlineAsciiDoc(input))
  })

  it('should convert plain text', () => {
    const input = 'text'
    assert.equal(unconvertInlineAsciiDoc(input), input)
  })

  it('should escape attribute reference', () => {
    const input = '{name}'
    assert.equal(unconvertInlineAsciiDoc(input), '\\' + input)
  })

  it('should escape attribute references in plain text', () => {
    const input = '{foo} and {bar}'
    assert.equal(unconvertInlineAsciiDoc(input), '\\{foo} and \\{bar}')
  })

  Object.entries({ code: '`', em: '_', mark: '#', span: '', strong: '*' }).forEach(([tagName, mark]) => {
    it(`should convert isolated ${tagName} tag`, () => {
      const input = `<${tagName}>text</${tagName}>`
      assert.equal(unconvertInlineAsciiDoc(input), `${mark}text${mark}`)
    })

    it(`should convert multiple ${tagName} tags`, () => {
      const input = `<${tagName}>some</${tagName}> <${tagName}>text</${tagName}>`
      assert.equal(unconvertInlineAsciiDoc(input), `${mark}some${mark} ${mark}text${mark}`)
    })

    it(`should convert ${tagName} tag with id`, () => {
      const input = `<${tagName} id="idname">text</${tagName}>`
      if (tagName === 'span') mark = '#'
      assert.equal(unconvertInlineAsciiDoc(input), `[#idname]${mark}text${mark}`)
    })

    it(`should convert ${tagName} tag with single class`, () => {
      const input = `<${tagName} class="rolename">text</${tagName}>`
      if (tagName === 'span') mark = '#'
      assert.equal(unconvertInlineAsciiDoc(input), `[.rolename]${mark}text${mark}`)
    })

    it(`should convert ${tagName} tag with multiple classes`, () => {
      const input = `<${tagName} class="role1 role2">text</${tagName}>`
      if (tagName === 'span') mark = '#'
      assert.equal(unconvertInlineAsciiDoc(input), `[.role1.role2]${mark}text${mark}`)
    })

    it(`should convert ${tagName} tag with id and classes`, () => {
      const input = `<${tagName} id="idname" class="role1 role2">text</${tagName}>`
      if (tagName === 'span') mark = '#'
      assert.equal(unconvertInlineAsciiDoc(input), `[#idname.role1.role2]${mark}text${mark}`)
    })
  })

  it('should convert isolated icon tag', () => {
    const input = '<span class="icon"><i class="fa fa-check-circle"></i></span>'
    assert.equal(unconvertInlineAsciiDoc(input), 'icon:check-circle[]')
  })

  it('should convert isolated img tag', () => {
    const input = '<span class="image"><img src="name.png" alt="description"></span>'
    assert.equal(unconvertInlineAsciiDoc(input), 'image:name.png[description]')
  })

  it('should convert img tag adjacent to custom span', () => {
    const input = '<span class="rolename">text</span> <span class="image"><img src="name.png" alt="description"></span>'
    assert.equal(unconvertInlineAsciiDoc(input), '[.rolename]#text# image:name.png[description]')
  })

  it('should escape attribute reference in tag', () => {
    const input = '<strong>{name}</strong>'
    assert.equal(unconvertInlineAsciiDoc(input), '*\\{name}*')
  })

  it('should convert nested tag', () => {
    const input = '<strong><em>strong with gusto</em></strong>'
    assert.equal(unconvertInlineAsciiDoc(input), '*_strong with gusto_*')
  })

  it('should convert deeply nested tag', () => {
    const input = '<strong><mark><em>cannot stress this enough</em></mark></strong>'
    assert.equal(unconvertInlineAsciiDoc(input), '*#_cannot stress this enough_#*')
  })

  it('should convert nested tag adjacent to text', () => {
    const input = '<strong>strong <em>with gusto</em></strong>'
    assert.equal(unconvertInlineAsciiDoc(input), '*strong _with gusto_*')
  })

  it('should convert img tag inside span', () => {
    const input = '<strong>click <img src="submit.png" alt="Submit"></strong>'
    assert.equal(unconvertInlineAsciiDoc(input), '*click image:submit.png[Submit]*')
  })

  it('should convert custom span in custom span', () => {
    const input = '<span class="role1">foo <span class="role2">bar</span> baz</span>'
    assert.equal(unconvertInlineAsciiDoc(input), '[.role1]#foo [.role2]##bar## baz#')
  })

  it('should use unconstrained marks when tag preceded by word character', () => {
    const input = 'show<strong>y</strong>'
    assert.equal(unconvertInlineAsciiDoc(input), 'show**y**')
  })

  it('should use unconstrained marks when tag followed by word character', () => {
    const input = '<strong>in</strong>direct'
    assert.equal(unconvertInlineAsciiDoc(input), '**in**direct')
  })

  it('should use unconstrained marks for tag nested in em tag', () => {
    const input = '<em><strong>gusto</strong></em>'
    assert.equal(unconvertInlineAsciiDoc(input), '_**gusto**_')
  })
})
