'use strict'

const { assert, assertx, beforeEach, describe, it } = require('@antora/assembler-test-harness')
const loadConfig = require('@antora/assembler/load-config')
const ospath = require('node:path')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures', ospath.basename(__filename, '.js'))

describe('loadConfig()', () => {
  let playbook

  beforeEach(() => {
    playbook = { dir: FIXTURES_DIR }
  })

  it('should load config from default file', async () => {
    const actual = await loadConfig(playbook)
    assert.equal(actual.constructor, Object)
    assert.deepEqual(actual.componentVersions, ['**'])
    assert.equal(actual.rootLevel, 1)
    assert.equal(actual.insertStartPage, false)
    assertx.hasProperty(actual.asciidoc, 'attributes')
    assertx.deepEqualSubset(actual.asciidoc.attributes, {
      sectnums: 'all',
      toc: '',
      doctype: 'book',
      'loader-assembler': '',
    })
    assertx.hasProperty(actual, 'build')
    assert.equal(actual.build.keepAggregateSource, true)
  })

  it('should set revdate AsciiDoc attribute if not specified', async () => {
    const actual = await loadConfig(playbook)
    assert.equal(actual.constructor, Object)
    assertx.hasProperty(actual.asciidoc?.attributes, 'revdate')
    const dateString = actual.asciidoc.attributes.revdate
    const dateTimestamp = +Date.parse(dateString)
    const now = new Date()
    const nowLocalMs = now - now.getTimezoneOffset() * 60000
    const delta = 24 * 60 * 60000
    //expect(dateTimestamp).to.be.closeTo(nowLocalMs, 24 * 60 * 60000)
    assert(dateTimestamp < nowLocalMs + delta)
    assert(dateTimestamp > nowLocalMs - delta)
  })

  it('should not camelCase names of AsciiDoc attributes when config is read from file', async () => {
    const actual = await loadConfig(playbook)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.asciidoc?.attributes['source-highlighter'], 'rouge')
  })

  it('should allow config file path to be specified', async () => {
    const actual = await loadConfig(playbook, './antora-assembler-config.yml')
    assert.equal(actual.constructor, Object)
    assert.deepEqual(actual.componentVersions, ['*'])
    assert.equal(actual.rootLevel, 0)
    assertx.hasProperty(actual.asciidoc, 'attributes')
    assertx.deepEqualSubset(actual.asciidoc.attributes, { 'source-highlighter': 'rouge', toc: '' })
    assertx.hasProperty(actual, 'build')
    assert.equal(actual.build.keepAggregateSource, true)
  })

  it('should return object with default values if default config file is not found', async () => {
    playbook.dir = ospath.dirname(FIXTURES_DIR)
    const actual = await loadConfig(playbook)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.insertStartPage, true)
    assert.equal(actual.rootLevel, 0)
    assert.equal(actual.sectionMergeStrategy, 'discrete')
    assert.deepEqual(actual.componentVersions, ['*'])
    assertx.hasProperty(actual.asciidoc, 'attributes')
    assert.equal(actual.asciidoc.attributes.doctype, 'book')
    assertx.hasProperty(actual, 'build')
    assert.equal(actual.build.dir, ospath.join(playbook.dir, 'build/assembler'))
    assert.equal(actual.build.cwd, playbook.dir)
    assert.equal(actual.build.publish, true)
    assert(actual.build.processLimit > 0)
  })

  it('should return object with default values if custom config file is not found', async () => {
    const actual = await loadConfig(playbook, './does-not-exist.yml')
    assert.equal(actual.constructor, Object)
    assert.equal(actual.insertStartPage, true)
    assert.equal(actual.rootLevel, 0)
    assert.equal(actual.sectionMergeStrategy, 'discrete')
    assert.deepEqual(actual.componentVersions, ['*'])
    assertx.hasProperty(actual.asciidoc, 'attributes')
    assertx.hasProperty(actual, 'build')
    assert.equal(actual.build.dir, ospath.join(playbook.dir, 'build/assembler'))
    assert.equal(actual.build.cwd, playbook.dir)
    assert.equal(actual.build.publish, true)
    assert(actual.build.processLimit > 0)
  })

  it('should accept custom config source', async () => {
    const input = { componentVersions: '**', sectionMergeStrategy: 'enclose' }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.sectionMergeStrategy, 'enclose')
    assert.deepEqual(actual.componentVersions, ['**'])
  })

  it('should not override specified revdate', async () => {
    const input = { asciidoc: { attributes: { revdate: '2020-01-01' } } }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.asciidoc.attributes.revdate, '2020-01-01')
  })

  it('should not camelCase keys in custom config source', async () => {
    const input = { section_merge_strategy: 'enclose' }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.section_merge_strategy, 'enclose')
    assert.equal(actual.sectionMergeStrategy, 'discrete')
  })

  it('should return undefined if value of enabled key is false', async () => {
    const input = { enabled: false }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual, undefined)
  })

  it('should coerce value of componentVersions key to array if specified as string', async () => {
    const input = { componentVersions: 'component-a' }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assertx.instanceOf(actual.componentVersions, Array)
    assert.deepEqual(actual.componentVersions, ['component-a'])
  })

  it('should coerce value of componentVersions key to array if specified as ventilated comma-separated string', async () => {
    const input = { componentVersions: '*, *@component-a' }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assertx.instanceOf(actual.componentVersions, Array)
    assert.deepEqual(actual.componentVersions, ['*', '*@component-a'])
  })

  it('should only split componentVersion string on commas followed by a space', async () => {
    const input = { componentVersions: '{1,2}.*@component-a, component-{b,c}' }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assertx.instanceOf(actual.componentVersions, Array)
    assert.deepEqual(actual.componentVersions, ['{1,2}.*@component-a', 'component-{b,c}'])
  })

  it('should ignore invalid value for section_merge_strategy key', async () => {
    const input = { sectionMergeStrategy: 'invalid' }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.sectionMergeStrategy, 'discrete')
  })

  it('should set value of build.clean key if not specified to value of output.clean key in playbook', async () => {
    const input = {}
    playbook.output = { clean: true }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.build.clean, true)
  })

  it('should not override value of build.clean key if specified', async () => {
    const input = { build: { clean: false } }
    playbook.output = { clean: true }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.build.clean, false)
  })

  it('should set value of build.processLimit key to Infinity if value is falsy', async () => {
    const input = { build: { processLimit: null } }
    const actual = await loadConfig(playbook, input)
    assert.equal(actual.constructor, Object)
    assert.equal(actual.build.processLimit, Infinity)
  })
})
