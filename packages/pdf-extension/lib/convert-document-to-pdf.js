'use strict'

const fs = require('node:fs')
const { promises: fsp } = fs
const LazyReadable = require('./util/lazy-readable')
const ospath = require('node:path')

// Q: how can we simplify this to avoid redundant code?
// Q: rename to convertDocumentToPDF?
async function convertDocumentToPdf (doc, buildConfig, runCommand) {
  const {
    asciidoc: { attributes: baseAttributes } = { attributes: {} },
    contents: stdin,
    src: { component, version, basename, extname = doc.extname },
  } = doc
  const { cwd = process.cwd(), dir = cwd } = buildConfig
  const command = buildConfig.command ?? (await scopeDefaultCommand(cwd))
  const padCharRef = process.platform === 'win32' && command.startsWith('bundle exec ')
  const docfile = `${version}@${component}::pdf$${basename}`
  const docname = basename.slice(0, basename.length - extname.length)
  const convertAttributes = Object.assign({}, baseAttributes, {
    docfile,
    docfilesuffix: extname,
    'docname@': docname,
    imagesdir: dir,
  })
  Object.assign(doc, { contents: null, extname: '.pdf', mediaType: 'application/pdf' })
  const extraArgs = Object.entries(convertAttributes).reduce((accum, [name, val]) => {
    if (val) {
      val = `${name}=${padCharRef && typeof val.charAt === 'function' && val.charAt() === '&' ? ' ' : ''}${val}`
    } else if (val === '') {
      val = name
    } else {
      val = `!${name}${val === false ? '=@' : ''}`
    }
    accum.push('-a', val)
    return accum
  }, [])
  const outputPath = ospath.join(dir, doc.path)
  // Q: should mkdirs be the default behavior?
  if (buildConfig.mkdirs) await fsp.mkdir(ospath.dirname(outputPath), { recursive: true, force: true })
  extraArgs.push('-o', outputPath, '-')
  return runCommand(command, extraArgs, { parse: true, cwd, stdin, stdout: 'print', stderr: 'print' }).then(() =>
    Object.assign(doc, { contents: new LazyReadable(() => fs.createReadStream(outputPath)), out: { path: doc.path } })
  )
}

function scopeDefaultCommand (cwd, baseCommand = 'asciidoctor-pdf') {
  return fsp.access(ospath.join(cwd, 'Gemfile.lock')).then(
    () => `bundle exec ${baseCommand}`,
    () => baseCommand
  )
}

module.exports = convertDocumentToPdf
