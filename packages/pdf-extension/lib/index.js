'use strict'

const convertDocumentToPdf = require('./convert-document-to-pdf')

module.exports.register = function ({ config: { configFile: configSource } }) {
  this.once('beforeProcess', ({ siteAsciiDocConfig }) => {
    siteAsciiDocConfig.keepSource = true
  })
  this.once('navigationBuilt', ({ playbook, contentCatalog, siteCatalog }) => {
    const { assembleContent } = (() => {
      try {
        return this.require('@antora/assembler') // when installed with Antora
      } catch {
        return require('@antora/assembler') // when installed separate from Antora, perhaps in project
      }
    })()
    return assembleContent.call(this, playbook, contentCatalog, convertDocumentToPdf, { configSource, siteCatalog })
  })
}
