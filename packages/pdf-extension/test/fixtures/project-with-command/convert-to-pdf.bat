@echo off
setlocal

REM echo %*

:parse
if "%~1" == "" (
  goto endparse
) else if "%~1" == "-a" (
  shift
  REM also fix value after =
  shift
) else if "%~1" == "-o" (
  set OUTPUT_FILE=%~2
  shift
)
shift
goto parse
:endparse

(
  echo.%%PDF-1.5
) > "%OUTPUT_FILE%"
