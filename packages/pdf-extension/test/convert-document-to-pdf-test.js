'use strict'

// Q: should extension export this function instead?
const convertDocumentToPdf = require('#convert-document-to-pdf')
const { assert, assertx, beforeEach, describe, it } = require('@antora/assembler-test-harness')
const fsp = require('node:fs/promises')
const os = require('node:os')
const ospath = require('node:path')
const { posix: path } = ospath
const { Readable } = require('node:stream')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')

describe('convertDocumentToPdf()', () => {
  const cwd = process.cwd()
  let doc
  let ran

  const dryRunCommand = async (command, argv, opts) => {
    ran = { command, argv, opts }
    return { status: 0 }
  }

  beforeEach(() => {
    doc = {
      extname: '.adoc',
      input: Buffer.from('= Assembled'),
      src: { component: 'the-component', version: '1.0', basename: 'assembled.adoc' },
    }
    Object.defineProperty(doc, 'path', {
      get: function () {
        return path.join(this.src.component, this.src.version, this.src.basename.replace(/\.adoc$/, this.extname))
      },
    })
    ran = undefined
  })

  it('should convert media type of doc from AsciiDoc to PDF', async () => {
    const actual = await convertDocumentToPdf(doc, {}, dryRunCommand)
    assert.equal(actual.extname, '.pdf')
    assert.equal(actual.mediaType, 'application/pdf')
    assertx.instanceOf(actual.contents, Readable)
    assert.match(actual.path, /\.pdf$/)
    assert.equal(actual.path, doc.path)
    assert.equal(actual.out.path, doc.path)
  })

  it('should use specified command', async () => {
    const command = 'asciidoctor-web-pdf'
    const expected = command
    await convertDocumentToPdf(doc, { command }, dryRunCommand)
    assert.equal(ran.command, expected)
  })

  it('should configure command to be parsed', async () => {
    const command = 'asciidoctor-web-pdf -a foo=bar'
    await convertDocumentToPdf(doc, { command }, dryRunCommand)
    assert.equal(ran.opts.parse, true)
  })

  it('should use default command if command not specified', async () => {
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    assert.equal(ran.command, 'asciidoctor-pdf')
  })

  it('should prepend bundle exec to default command if Gemfile.lock is detected at cwd', async () => {
    const cwd = ospath.join(FIXTURES_DIR, 'project-with-gemfile')
    await convertDocumentToPdf(doc, { cwd }, dryRunCommand)
    assert.equal(ran.command, 'bundle exec asciidoctor-pdf')
  })

  it('should compute and pass intrinsic attributes to command', async () => {
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    const attributeOptions = {}
    let previousArg
    ran.argv.forEach((arg) => {
      if (previousArg === '-a') {
        const [name, val] = arg.split('=')
        attributeOptions[name] = val
      }
      previousArg = arg
    })
    assertx.deepEqualSubset(attributeOptions, {
      docfile: '1.0@the-component::pdf$assembled.adoc',
      docfilesuffix: '.adoc',
      'docname@': 'assembled',
    })
  })

  it('should pass attributes on doc to command', async () => {
    const attributes = { icons: 'font', 'pdf-theme': 'default-with-font-fallbacks', sectnums: '', sectnumlevels: 2 }
    const expected = Object.assign({}, attributes)
    doc.asciidoc = { attributes }
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    const attributeOptions = {}
    let previousArg
    ran.argv.forEach((arg) => {
      if (previousArg === '-a') {
        if (~arg.indexOf('=')) {
          const [name, val] = arg.split('=', 2)
          attributeOptions[name] = val
          if (name in expected && typeof expected[name] !== 'string') expected[name] = String(expected[name])
        } else {
          attributeOptions[arg] = ''
        }
      }
      previousArg = arg
    })
    assertx.deepEqualSubset(attributeOptions, expected)
  })

  it('should pass contents of input document to stdin of command', async () => {
    const stdin = doc.contents
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    assert.equal(ran.opts.stdin, stdin)
    assert.equal(ran.argv.at(-1), '-')
  })

  it('should set cwd of command to process.cwd() by default', async () => {
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    assert.equal(ran.opts.cwd, cwd)
  })

  it('should set cwd of command to cwd specified in buildConfig', async () => {
    const cwd = '/path/to/project'
    await convertDocumentToPdf(doc, { cwd }, dryRunCommand)
    assert.equal(ran.opts.cwd, cwd)
  })

  it('should compute output and imagesdir relative to process.cwd() by default', async () => {
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    const dir = cwd
    const expected = ospath.join(dir, doc.path)
    let outputPath
    let imagesdir
    let previousArg
    ran.argv.forEach((arg) => {
      if (previousArg === '-o') {
        outputPath = arg
      } else if (previousArg === '-a' && arg.startsWith('imagesdir=')) {
        imagesdir = arg.slice(10)
      }
      previousArg = arg
    })
    assert.equal(imagesdir, dir)
    assert.equal(outputPath, expected)
  })

  it('should compute output and imagesdir relative to dir specified in buildConfig', async () => {
    const dir = '/path/to/project/build/assembler'
    await convertDocumentToPdf(doc, { dir }, dryRunCommand)
    const expected = ospath.join(dir, doc.path)
    let imagesdir
    let outputPath
    let previousArg
    ran.argv.forEach((arg) => {
      if (previousArg === '-o') {
        outputPath = arg
      } else if (previousArg === '-a' && arg.startsWith('imagesdir=')) {
        imagesdir = arg.slice(10)
      }
      previousArg = arg
    })
    assert.equal(imagesdir, dir)
    assert.equal(outputPath, expected)
  })

  it('should not eagerly make output directory by default', async () => {
    const projectDir = await fsp.mkdtemp(ospath.join(os.tmpdir(), 'antora-assembler-'))
    try {
      const dir = ospath.join(projectDir, 'build/assembler')
      await convertDocumentToPdf(doc, { dir }, dryRunCommand)
      const expected = ospath.dirname(ospath.join(dir, doc.path))
      assert.equal(
        await fsp.stat(expected).then(
          (stat) => stat.isDirectory(),
          () => false
        ),
        false
      )
    } finally {
      await fsp.rm(projectDir, { force: true, recursive: true })
    }
  })

  it('should eagerly make output directory if mkdirs option is true', async () => {
    const projectDir = await fsp.mkdtemp(ospath.join(os.tmpdir(), 'antora-assembler-'))
    try {
      const dir = ospath.join(projectDir, 'build/assembler')
      await convertDocumentToPdf(doc, { dir, mkdirs: true }, dryRunCommand)
      const expected = ospath.dirname(ospath.join(dir, doc.path))
      assert.equal(
        await fsp.stat(expected).then(
          (stat) => stat.isDirectory(),
          () => false
        ),
        true
      )
    } finally {
      await fsp.rm(projectDir, { force: true, recursive: true })
    }
  })

  it('should generate PDF file and populate contents of virtual file with lazy read stream', async () => {
    const runCommand = require('#run-command')
    const projectDir = ospath.join(FIXTURES_DIR, 'project-with-command')
    const buildDir = await fsp.mkdtemp(ospath.join(projectDir, 'antora-assembler-'))
    try {
      const buildConfig = { command: './convert-to-pdf', cwd: projectDir, dir: buildDir, mkdirs: true }
      doc.asciidoc = { attributes: { lf: '&#10;' } }
      const expected = '%PDF-1.5' + os.EOL
      const convertedDoc = await convertDocumentToPdf(doc, buildConfig, runCommand)
      const contents = convertedDoc.contents
      assertx.instanceOf(contents, Readable)
      assert.equal(contents.readable, true)
      assert.equal(contents.readableDidRead, false)
      assert.equal(contents.readableFlowing, null)
      const chunks = []
      for await (const chunk of contents) chunks.push(chunk)
      assert.equal(contents.readableDidRead, true)
      const actual = chunks.join('')
      assert.equal(actual, expected)
    } finally {
      await fsp.rm(buildDir, { force: true, recursive: true })
    }
  })
})
