'use strict'

const { assert, assertx, beforeEach, describe, it } = require('@antora/assembler-test-harness')

describe('pdf-extension', () => {
  const ext = require('@antora/pdf-extension')

  const createGeneratorContext = () => ({
    once (eventName, fn) {
      this[eventName] = fn
    },
    require,
  })

  let generatorContext, registerVars

  describe('bootstrap', () => {
    beforeEach(() => {
      generatorContext = createGeneratorContext()
      registerVars = { config: {} }
    })

    it('should be able to require extension', () => {
      assertx.instanceOf(ext, Object)
      assertx.instanceOf(ext.register, Function)
    })

    it('should set keepSource on AsciiDoc config during beforeProcess event', () => {
      ext.register.call(generatorContext, registerVars)
      const siteAsciiDocConfig = {}
      generatorContext.beforeProcess({ siteAsciiDocConfig })
      assert.equal(siteAsciiDocConfig.keepSource, true)
    })

    it('should be able to call register function exported by extension', () => {
      ext.register.call(generatorContext, registerVars)
      assertx.instanceOf(generatorContext.navigationBuilt, Function)
    })

    it('should call assembleContent function when navigationBuilt event is emitted', () => {
      ext.register.call(generatorContext, registerVars)
      const required = []
      let assembleContentArguments
      // biome-ignore lint/complexity/useArrowFunction: to capture arguments
      generatorContext.require = function (id) {
        required.push(id)
        return {
          assembleContent () {
            assembleContentArguments = arguments
          },
        }
      }
      generatorContext.navigationBuilt({})
      assert.equal(required.length, 1)
      assert.equal(required[0], '@antora/assembler')
      assertx.instanceOf(assembleContentArguments[2], Function)
      assertx.instanceOf(assembleContentArguments[3], Object)
      assert.equal(assembleContentArguments[3].configSource, undefined)
    })

    it('should allow configFile to be overridden by extension configuration', () => {
      registerVars.config.configFile = './pdf-config.yml'
      ext.register.call(generatorContext, registerVars)
      let assembleContentArguments
      // biome-ignore lint/complexity/useArrowFunction: to capture arguments
      generatorContext.require = function () {
        return {
          assembleContent () {
            assembleContentArguments = arguments
          },
        }
      }
      generatorContext.navigationBuilt({})
      assertx.instanceOf(assembleContentArguments[3], Object)
      assert.equal(assembleContentArguments[3].configSource, './pdf-config.yml')
    })
  })
})
