'use strict'

process.env.NODE_ENV = 'test'

const assert = require('node:assert/strict')
const assertx = require('./assertx')
const { describe, before, beforeEach, after, afterEach, it } = require('node:test')
const { configureLogger } = require('@antora/logger')
const createFile = require('./create-file')
const heredoc = require('./heredoc')
const loadScenario = require('./load-scenario')
const runScenario = require('./run-scenario')

beforeEach(() => configureLogger({ level: 'all' })) // eslint-disable-line no-undef

module.exports = {
  after,
  afterEach,
  assert,
  assertx,
  before,
  beforeEach,
  createFile,
  describe,
  heredoc,
  it,
  loadScenario,
  runScenario,
}
