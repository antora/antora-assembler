'use strict'

const assert = require('node:assert/strict')

const assertx = {
  bufferEqual (actual, expected, message) {
    assert(actual instanceof Buffer)
    if (Object.keys(actual).map(Number).filter(Number.isNaN).length) actual = actual.slice()
    try {
      assert.deepEqual(actual, expected, message)
    } catch (ex) {
      if (ex instanceof assert.AssertionError) {
        try {
          assert.equal(actual.toString(), expected.toString(), message)
        } catch (reEx) {
          const message = ['Expected buffers to be equal:', ...reEx.message.split('\n').slice(1)].join('\n')
          throw new assert.AssertionError({ message, actual, expected, operator: 'bufferEqual' })
        }
      }
      throw ex
    }
  },
  deepEqualSubset: (actual, expected, msg) =>
    assert.deepEqual(sliceObject(actual, ...Object.keys(expected)), expected, msg),
  doesNotHaveProperty: (actual, expected) =>
    assert(!(expected in actual), `Expected ${actual} to not have property '${expected}'`),
  empty: (actual) => assert.equal(actual.length, 0, `Expected ${actual} to be empty`),
  exist: (actual) => assert.notEqual(actual, null, `Expected ${actual} to exist`),
  hasProperty: (actual, expected) =>
    assert(actual != null && expected in actual, `Expected ${actual} to have property '${expected}'`),
  instanceOf: (actual, expected) =>
    assert(actual instanceof expected, `Expected ${actual} to be an instance of ${expected}`),
  notEmpty: (actual) => assert.notEqual(actual.length, 0, `Expected ${actual} to not be empty`),
}

function sliceObject (obj, ...keys) {
  const result = {}
  for (const k of keys) result[k] = obj[k]
  return result
}

module.exports = assertx
