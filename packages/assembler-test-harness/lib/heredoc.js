'use strict'

/**
 * Define a multiline template string with only significant indentation and newlines preserved.
 */
function heredoc (literals, ...values) {
  const str =
    literals.length > 1
      ? values.reduce((accum, value, idx) => accum + value + literals[idx + 1], literals[0])
      : literals[0]
  const lines = str.trimRight().split(/^/m)
  if (lines.length < 2) return str
  if (lines[0] === '\n') lines.shift()
  const indentRx = /^ +/
  const indentSize = Math.min(...lines.filter((l) => l.startsWith(' ')).map((l) => l.match(indentRx)[0].length))
  return (indentSize ? lines.map((l) => (l.startsWith(' ') ? l.slice(indentSize) : l)) : lines).join('')
}

module.exports = heredoc
