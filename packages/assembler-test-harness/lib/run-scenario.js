'use strict'

const produceAggregateDocuments = require('@antora/assembler/produce-aggregate-documents')
const assert = require('node:assert/strict')
const assertx = require('./assertx')
const fsp = require('node:fs/promises')
const loadScenario = require('./load-scenario')
const ospath = require('node:path')

// Q: does this belong in @antora/assembler/test/test-helper.js?
async function runScenario (name, dirname) {
  const { loadAsciiDoc, dir, contentCatalog, assemblerConfig } = await loadScenario(name, dirname)
  const results = produceAggregateDocuments(loadAsciiDoc, contentCatalog, assemblerConfig)
  assertx.exist(results)
  assertx.notEmpty(results)
  for (const result of results) {
    assertx.hasProperty(result, 'src')
    const expected = await fsp.readFile(ospath.join(dir, 'expects', result.src.basename)).catch(() => Buffer.alloc(0))
    assertx.bufferEqual(result.contents, expected)
  }
  return results
}

module.exports = runScenario
